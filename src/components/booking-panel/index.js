import style from './style';
import { h, Component } from 'preact';
import Debug from '../../lib/services/Debug';
import connectToStores from 'alt-utils/lib/connectToStores';
import AuthStore from '../../lib/stores/AuthStore';
import AuthActions from '../../lib/actions/AuthActions';
import { Link } from 'preact-router/match';
let console = Debug.stream('ProfilePanel');
import { Layout, TextField, Spinner, Button, Card, CardText, CardActions, Grid, Cell } from 'preact-mdl';
@connectToStores
export default class ProfilePanel extends Component {
    static getStores() {
        return [AuthStore];
    }

    static getPropsFromStores() {
        return AuthStore.getState();
    }
    componentDidUpdate() {
        console.log('componentDidUpdate');
        if (this.props.user && !this.state.initialized) {
            console.log('componentDidUpdate.initialize', this.props.user);
            this.setState({
                initialized: true,
                email: this.props.user.email,
                pwd: ''
            });
        }
    }
    render({ user }, { email, pwd }) {
        return (
            <div class={style.profilePanel}>
                <Card class={style.profile__cardWide+' mdl-shadow--2dp'}>
                  <Grid class={style.profile__grid}>
                    <Cell class="mdl-cell--6-col-desktop mdl-cell--middle">
                        <Link href="/monetize-my-livingspace-address">
                            <h1 class={style.booking__title}>Monetize</h1>                        
                        </Link> 
                        <p class={style.booking__subtitle}>Become a Caretaker</p>
                    </Cell>
                    <Cell class="mdl-cell--6-col-desktop mdl-cell--middle">
                        <Link href="/store-my-belongings-address">
                            <h1 class={style.booking__title}>Store</h1>
                        </Link>
                        <p class={style.booking__subtitle}>Become a Nomad</p>
                    </Cell>
                  </Grid>
                </Card>
            </div>


        );
    }
}
