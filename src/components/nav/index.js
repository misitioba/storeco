import { h, Component } from 'preact';
import { Link } from 'preact-router/match';
import style from './style';
import connectToStores from 'alt-utils/lib/connectToStores';
import AuthStore from '../../lib/stores/AuthStore';
import AuthActions from '../../lib/actions/AuthActions';
import Debug from '../../lib/services/Debug';
var console = Debug.stream('Header');
@connectToStores
export default class Header extends Component {
    constructor() {
        super();
        AuthActions.loadLocalUser();
    }
    static getStores() {
        return [AuthStore];
    }

    static getPropsFromStores() {
        return AuthStore.getState();
    }
    render() {
        //<a class="mdl-navigation__link" href="">Link</a>
        var dashboard = this.props.user ? (<Link class="mdl-navigation__link" activeClassName={style.active} href="/dashboard">Dashboard</Link>) : '';
        var login = !this.props.user ? (<Link class="mdl-navigation__link" activeClassName={style.active} href="/login">Login</Link>) : '';
        var logout = this.props.user ? (<Link class="mdl-navigation__link" activeClassName={style.active} href="/logout">Logout</Link>) : '';
        return (
            <nav class="mdl-navigation">
                <Link class="mdl-navigation__link" activeClassName={style.active} href="/">Home</Link>
			    <Link class="mdl-navigation__link" activeClassName={style.active} href="/about">About</Link>
				{login}
				{dashboard}
				{logout}	       
			</nav>
        );
    }
}
