import style from './style';
import { h, Component } from 'preact';
import Debug from '../../lib/services/Debug';
import connectToStores from 'alt-utils/lib/connectToStores';
import AuthStore from '../../lib/stores/AuthStore';
import AuthActions from '../../lib/actions/AuthActions';
let console = Debug.stream('ProfilePanel');
import { Layout, TextField, Spinner, Button, Card, CardText, CardActions, Grid, Cell } from 'preact-mdl';
@connectToStores
export default class ProfilePanel extends Component {
    static getStores() {
        return [AuthStore];
    }

    static getPropsFromStores() {
        return AuthStore.getState();
    }
    componentDidUpdate() {
        console.log('componentDidUpdate');
        if (this.props.user && !this.state.initialized) {
            console.log('componentDidUpdate.initialize', this.props.user);
            this.setState({
                initialized: true,
                email: this.props.user.email,
                pwd: ''
            });
        }
    }
    render({ user }, { email, pwd }) {
        return (
            <div class={style.profilePanel}>
                <Card class={style.profile__cardWide+' mdl-shadow--2dp'}>
                  <Grid class={style.profile__grid}>
                    <Cell class="mdl-cell--2-col-desktop">
                        <TextField disabled label="Email" value={email} onInput={this.linkState('email')}/>
                    </Cell>
                    <Cell class="mdl-cell--2-col-desktop">
                        <TextField label="Password" value={pwd} onInput={this.linkState('pwd')}/>
                    </Cell>
                    <Cell class="mdl-cell--middle">
                        <Button class="mdl-button--accent">
                          Change password
                        </Button>
                    </Cell>
                  </Grid>
                  <CardActions class="mdl-card--border">
                    <Button class="mdl-button--primary">
                      Save changes
                    </Button>
                  </CardActions>
                </Card>
            </div>


        );
    }
}
