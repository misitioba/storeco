import { h, Component } from 'preact';
import { Link } from 'preact-router/match';
import style from './style';

import connectToStores from 'alt-utils/lib/connectToStores';
import AuthStore from '../../lib/stores/AuthStore';
import AuthActions from '../../lib/actions/AuthActions';

import Debug from '../../lib/services/Debug';
var console = Debug.stream('Header');

@connectToStores
export default class Header extends Component {
	constructor() {
		super();
		AuthActions.loadLocalUser();
	}
	static getStores() {
		return [AuthStore];
	}

	static getPropsFromStores() {
		return AuthStore.getState();
	}
	render() {
		var dashboard = this.props.user ? (<Link activeClassName={style.active} href="/dashboard">Dashboard</Link>) : '';
		var login = !this.props.user ? (<Link activeClassName={style.active} href="/login">Login</Link>) : '';
		var logout = this.props.user ? (<Link activeClassName={style.active} href="/logout">Logout</Link>) : '';
		/*
		<Link activeClassName={style.active} href="/profile">Me</Link>
					<Link activeClassName={style.active} href="/profile/john">John</Link>
		*/
		return (
			<header class={style.header}>
				<Link href="/"><h1>Storeco</h1></Link>
				
				<nav>
					<Link activeClassName={style.active} href="/about">About</Link>
					{login}
					{dashboard}
					{logout}
				</nav>
			</header>
		);
	}
}
