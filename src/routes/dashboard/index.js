import { h, Component } from 'preact';
import style from './style';
import { Helmet } from "react-helmet";
import connectToStores from 'alt-utils/lib/connectToStores';
import AuthStore from '../../lib/stores/AuthStore';
import AuthActions from '../../lib/actions/AuthActions';
import Debug from '../../lib/services/Debug';
import ProfilePanel from '../../components/profile-panel';
let console = Debug.stream('Dashboard');
import { route } from '../../lib/services/Router';
@connectToStores
export default class Dashboard extends Component {
	static getStores() {
		return [AuthStore];
	}

	static getPropsFromStores() {
		return AuthStore.getState();
	}
	componentWillMount() {
		AuthActions.redirectIfNotLogged('/login');
	}
	render() {
		return (
			<div class={style.dashboard}>
				<Helmet>
                	<title>Storeco: My Panel</title>
            	</Helmet>
            	<h3>Dashboard</h3>
				<ProfilePanel/>
			</div>
		);
	}
}
