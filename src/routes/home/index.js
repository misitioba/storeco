import { h, Component } from 'preact';
import style from './style';
import { Helmet } from "react-helmet";
import BookingPanel from '../../components/booking-panel';
import Debug from '../../lib/services/Debug';
let console = Debug.stream('Dashboard');
import { Layout, TextField, Spinner, Button, Card, CardText, CardActions, Grid, Cell } from 'preact-mdl';
export default class Home extends Component {

	render() {
		return (
			<div class={style.home}>
				<Helmet>
                	<title>Storeco: Home</title>
            	</Helmet>
				<h3>The first collaborative storage solution</h3>
				
				
			
				<BookingPanel/>
					
				<Card class="mdl-shadow--2dp wide">
					<Grid class="grid-wide ">
						<Cell class="mdl-cell--6-col-desktop mdl-cell--middle">
								<h5 class={style.card__text}>133.756 world users</h5>
						</Cell>
						<Cell class="mdl-cell--6-col-desktop mdl-cell--middle">
								<h5 class={style.card__text}>417 Paris users</h5>
						</Cell>
					</Grid>
				</Card>
				
				
				
				<Card class="mdl-shadow--2dp wide">
					<Grid class="grid-wide">
						<Cell class="mdl-cell--12-col-desktop mdl-cell--middle mdl-cell--4-col-phone mdl-cell--8-col-tablet">
							<a  href="tel:+06758945"><h2 class={style.card__text_phone}>Call us today 06758945</h2></a>
						</Cell>
					</Grid>
				</Card>
				
				<div class={style.home__photo}></div>
				
				
			</div>
		);
	}
}
