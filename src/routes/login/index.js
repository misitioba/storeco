import { h, Component } from 'preact';
import style from './style';
import { Helmet } from "react-helmet";
import 'linkstate/polyfill';
import connectToStores from 'alt-utils/lib/connectToStores';
import AuthStore from '../../lib/stores/AuthStore';
import AuthActions from '../../lib/actions/AuthActions';
import { route } from 'preact-router';
import { Layout, TextField, Spinner, Button } from 'preact-mdl';
import Debug from '../../lib/services/Debug';
var console = Debug.stream('Login');

@connectToStores
class AuthPanel extends Component {
	constructor() {
		super();
	}
	static getStores() {
		return [AuthStore];
	}

	static getPropsFromStores() {
		return AuthStore.getState();
	}
	handleLogin(email, pwd) {
		console.log('handleLogin', email, pwd);
		AuthActions.login(email, pwd);
	}
	handleSignUp(e) {
		console.log('handleSignUp', this.state);
		AuthActions.signUp(this.state.email, this.state.pwd);
	}
	componentWillMount(){
	    
	}
	componentDidUpdate(){
	    if(this.props.user){
	        setTimeout(function(){
	            route('/dashboard');
	        },200);
	    }
	}
	render({}, { email, pwd }) {

	
		var errorMessage = (<p class="error-message">{this.props.errorMessage}</p>);
		return (
			<div>
			
		
    		
		<div class="mdl-grid">
			
			<div class="mdl-cell mdl-cell--4-col mdl-cell--4-offset-desktop mdl-cell--12-col-phone mdl-cell--4-col-tablet mdl-cell--2-offset-tablet">
			
				<TextField class="mdl-cell mdl-cell--12-col"
				type="text"
				label="Email" value={email} onInput={this.linkState('email')}
				/>
				<TextField class="mdl-cell mdl-cell--12-col"
				type="password"
				label="Pwd" value={pwd} onInput={this.linkState('pwd')}
				/>
				
				<Button class="mdl-cell mdl-cell--12-col" onClick={()=>this.handleLogin.apply(this,[email,pwd])}>
				Login
				</Button>
				<Button class="mdl-cell mdl-cell--12-col" onClick={()=>this.handleSignUp.apply(this)}>Create</Button>
				
    			{errorMessage}
			</div>
			
		</div>
    		
    		
    	</div>
		);
	}
}



export default class Login extends Component {

	render() {
		return (
			<div class={style.login}>
				<Helmet>
                	<title>Storeco: Login</title>
            	</Helmet>
				<AuthPanel />
			</div>
		);
	}
}
