import { h, Component } from 'preact';
import connectToStores from 'alt-utils/lib/connectToStores';
import AuthStore from '../../lib/stores/AuthStore';
import AuthActions from '../../lib/actions/AuthActions';
import { route } from '../../lib/services/Router';
import createLogger from 'debug';
import Debug from '../../lib/services/Debug';
var console = Debug.stream('Logout');

@connectToStores
export default class Logout extends Component {
    constructor() {
        super();
        AuthActions.logout();

    }
    static getStores() {
        return [AuthStore];
    }
    static getPropsFromStores() {
        return AuthStore.getState();
    }
    componentDidUpdate() {
        if (!this.props.user) {
            console.log('componentDidUpdate: User removed, logging out');
            route('/login');
        }
    }
    render() {
        return <div></div>;
    }
}
