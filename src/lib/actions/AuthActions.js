var firebase = require('firebase');
var alt = require('../alt');
var console = require('../../lib/services/Debug').default.stream('AuthActions');
var Auth = require('../../lib/models/Auth').default;
let route = require('../../lib/services/Router').route;

function catchErrorMiddleware(dispatch) {
    return function(error) {
        console.error(error);
        dispatch(null);
    }
}

function saveLocallyMiddleware(dispatch) {
    return function(user) {
        Auth.saveLocally(user).then(dispatch).catch(catchErrorMiddleware(dispatch))
    }
}

class AuthActions {
    redirectIfNotLogged(routeName) {
        return (dispatch) => {
            Auth.isLogged().then(logged => {
                if (!logged) {
                    route(routeName);
                }
            });
        };
    }
    loadLocalUser() {
        console.log('loadLocalUser');
        return (dispatch) => {
            Auth.getLocally().then(user => {
                console.log('loadLocalUser:success!');
                dispatch(user);
            }).catch(catchErrorMiddleware(dispatch));
        };
    }
    logout() {
        console.log('logout');
        return (dispatch) => {
            Auth.removeLocally().then(user => {
                console.log('logout:removeLocally:success!');
                dispatch(null);
            }).catch(catchErrorMiddleware(dispatch));
        };
    }
    signUpFailed(error) {
        return error;
    }
    signUp(email, pwd) {
        return (dispatch) => {
            try {
                firebase.auth().createUserWithEmailAndPassword(email, pwd).then(saveLocallyMiddleware(dispatch)).catch(this.signUpFailed);
            }
            catch (error) {
                this.signUpFailed(error);
            }
        };
    }
    login(email, pwd) {
        console.log('login', email, pwd);

        var onReject = (error) => {
            this.loginFailed(error.message);
            console.warn('login.reject', error);
        }

        return (dispatch) => {

            console.log('login.resolve');

            try {
                firebase.auth().signInWithEmailAndPassword(email, pwd)
                    .then(saveLocallyMiddleware(dispatch))
                    .catch((error) => {
                        onReject(error);
                    });
            }
            catch (err) {
                onReject(err)
            }



        };
    }
    loginFailed(errorMessage) {
        console.log('loginFailed', errorMessage);
        return errorMessage;
    }
}

module.exports = alt.createActions(AuthActions);
