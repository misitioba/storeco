import localforage from 'localforage';
import Debug from './Debug';
let console = Debug.stream('Cache');

localforage.config({
    //driver      : localforage.WEBSQL, // Force WebSQL; same as using setDriver()
    name: window.location.hostname,
    version: 1.0,
    //size        : 4980736, // Size of database, in bytes. WebSQL-only for now.
    storeName: window.location.hostname.replace(new RegExp('.', 'g'), '_'), // Should be alphanumeric, with underscores.
    description: 'some description'
});

class CacheInstance {
    constructor(name) {
        this.name = name;
        this.console = Debug.stream('Cache:' + name);
    }
    remove(identifier) {
        this.console.log('remove', identifier);
        return localforage.removeItem(this.name + '_' + identifier);
    }
    get(identifier) {
        this.console.log('get', identifier);
        return localforage.getItem(this.name + '_' + identifier);
    }
    set(identifier, value) {
        this.console.log('set', identifier);
        return localforage.setItem(this.name + '_' + identifier, value);
    }
}

class Cache {
    static workers = {};
    get(name) {
        if (typeof Cache.workers[name] === 'undefined') {
            Cache.workers[name] = new CacheInstance(name);
        }
        return Cache.workers[name];
    }
}
export default (new Cache());
