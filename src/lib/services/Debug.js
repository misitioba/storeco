import createLogger from 'debug';
class Debug {
    constructor() {
        this.loggers = {};
    }
    getLogger(streamName, level) {
        if (typeof this.loggers[streamName + ':' + level] === 'undefined') {
            this.loggers[streamName + ':' + level] = createLogger('app:' + level + ':' + streamName);
        }
        return this.loggers[streamName + ':' + level];
    }
    stream(streamName) {
        var self = this;
        var logger = function() {
            var args = Array.prototype.slice.call(arguments);
            var _logger = self.getLogger(streamName, 'log');
            _logger.apply(_logger, args);
        };
        logger.log = function() {
            var args = Array.prototype.slice.call(arguments);
            var _logger = self.getLogger(streamName, 'log');
            _logger.apply(_logger, args);
        };
        logger.error = function() {
            var args = Array.prototype.slice.call(arguments);
            if (typeof args[0].stack !== 'undefined') {
                args[0] = args[0] + ' STACK: ' + args[0].stack;
            }
            var _logger = self.getLogger(streamName, 'error');
            _logger.apply(_logger, args);
        };
        logger.info = function() {
            var args = Array.prototype.slice.call(arguments);
            var _logger = self.getLogger(streamName, 'info');
            _logger.apply(_logger, args);
        };
        logger.warn = function() {
            var args = Array.prototype.slice.call(arguments);
            var _logger = self.getLogger(streamName, 'warn');
            _logger.apply(_logger, args);
        };
        return logger;
    }
    setWildcard(wildcard) {
        if (window && window.localStorage) {
            window.localStorage.debug = wildcard;
            this.getLogger('Debug', 'info')('Logging set to ', wildcard);
        }
        return this;
    }
}
export default (new Debug());
