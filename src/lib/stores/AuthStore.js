var alt = require('../alt');
var AuthActions = require('../actions/AuthActions');
var console = require('../../lib/services/Debug').default.stream('AuthSource');



function parseUser(user) {
    return {
        email: user.email
    };
}


class AuthStore {
    constructor() {
        this.user = null;
        this.errorMessage = null;
        this.bindListeners({
            login: AuthActions.LOGIN,
            loginFailed: AuthActions.LOGIN_FAILED,
            signUp: AuthActions.signUp,
            signUpFailed: AuthActions.signUpFailed,
            logout: AuthActions.logout,
            loadLocalUser: AuthActions.loadLocalUser
        });
        this.exportPublicMethods({

        });
    }

    loadLocalUser(user) {
        this.user = user;
    }

    logout() {
        this.user = null;
    }

    signUp(user) {

        this.user = parseUser(user);
        this.errorMessage = null;
    }
    signUpFailed(error) {
        this.errorMessage = error.message;
    }

    login(user) {

        this.user = parseUser(user);
        this.errorMessage = null;
    }


    loginFailed(errorMessage) {
        console.log('handleLoginFailed', errorMessage);
        this.errorMessage = errorMessage;
    }
}

module.exports = alt.createStore(AuthStore, 'AuthStore');
