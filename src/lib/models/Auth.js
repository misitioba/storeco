var cache = require('../../lib/services/Cache').default.get('Auth');
var console = require('../../lib/services/Debug').default.stream('Auth');
class Auth {
    isLogged() {
        return new Promise((resolve, reject) => {
            cache.get('user').then(user => {
                console.log('isLogged', user ? true : false);
                if (user) {
                    resolve(true);
                }
                else {
                    resolve(false);
                }
            }).catch(reject);
        });
    }
    removeLocally() {
        return new Promise((resolve, reject) => {
            cache.remove('user').then((user) => {
                console.log('remove success!');
                resolve(user);
            }).catch(reject);
        });
    }
    getLocally(user) {
        return new Promise((resolve, reject) => {
            cache.get('user').then((user) => {
                console.log('get success!');
                resolve(user);
            }).catch(reject);
        });
    }
    saveLocally(user) {
        return new Promise((resolve, reject) => {
            cache.set('user', this._parseUserData(user)).then(() => {
                console.log('save success!');
                resolve(true);
            }).catch(reject);
        });
    }
    _parseUserData(user) {
        console.log('parsing', user);
        return {
            email: user.email
        }
    }
}
export default (new Auth());
